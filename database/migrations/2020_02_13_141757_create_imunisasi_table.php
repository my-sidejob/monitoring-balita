<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImunisasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imunisasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal_imunisasi');
            $table->string('keterangan')->nullable();
            $table->bigInteger('jenis_imunisasi_id')->unsigned();
            $table->bigInteger('balita_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            $table->foreign('jenis_imunisasi_id')->references('id')->on('jenis_imunisasi')->onUpdate('cascade');
            $table->foreign('balita_id')->references('id')->on('balita')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imunisasi');
    }
}
