<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVitaminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vitamin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_vitamin');
            $table->date('tanggal');
            $table->string('keterangan')->nullable();
            $table->bigInteger('balita_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            $table->foreign('balita_id')->references('id')->on('balita')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vitamin');
    }
}
