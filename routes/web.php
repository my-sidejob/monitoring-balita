<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/', 'ParentLoginController@showLoginForm')->name('parent.showLoginForm');


Route::post('/ortu_login', 'ParentLoginController@login')->name('parent.login');
Route::post('/parent/logout', 'ParentLoginController@logout')->name('parent.logout');

Route::middleware('auth.parent:parent')->group(function(){
    Route::get('/parent/dashboard', 'DashboardController@index');
    Route::get('balita-anda', 'BalitaController@parentBabies');
    Route::get('pertumbuhan-balita-anda', 'PertumbuhanController@index')->name('pertumbuhan-balita-anda');
    Route::get('imunisasi-balita-anda', 'ImunisasiController@index')->name('imunisasi-balita-anda');
    Route::get('vitamin-balita-anda', 'VitaminController@index')->name('vitamin-balita-anda');
    Route::get('ubah-password', 'UbahPasswordController@index')->name('ubah-password');
    Route::post('ubah-password', 'UbahPasswordController@update')->name('ubah-password.update');
});
 
Auth::routes();

Route::middleware(['auth:web'])->group(function(){
    
    Route::resource('jenis_imunisasi', 'JenisImunisasiController');    
    Route::resource('orangtua', 'OrangtuaController');
    Route::resource('balita', 'BalitaController');
    Route::resource('user', 'UserController');
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');    

    Route::resource('pertumbuhan', 'PertumbuhanController');
    Route::resource('imunisasi', 'ImunisasiController');
    Route::resource('vitamin', 'VitaminController');

    Route::get('laporan', 'LaporanController@index')->name('laporan.index');

    Route::get('print-kunjungan/{jenis}/{tahun}', 'LaporanController@print')->name('print-kunjungan');
});
