@extends('layouts.app')

@section('title', 'Form Pemberian Vitamin Balita')

@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Data Pemberian Vitamin Balita</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($vitamin->id)) ? route('vitamin.store') : route('vitamin.update', $vitamin->id) }}" method="post">
                    @csrf
                    @isset($vitamin->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama Balita</label>
                        <input type="hidden" name="balita_id" value="{{ $balita->id }}">
                        <input type="text" placeholder="Masukan Nama Lengkap Balita" class="form-control" name="nama_lengkap" value="{{ $balita->nama_lengkap }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" placeholder="Pilih Tanggal" class="form-control datepicker" name="tanggal" value="{{ old('tanggal', $vitamin->tanggal) }}">
                    </div>

                    <div class="form-group">
                        <label>Nama Vitamin</label>
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">Vitamin</span>
                            </div>
                            <input type="text" class="form-control" name="nama_vitamin" value="{{ old('nama_vitamin', $vitamin->nama_vitamin) }}" placeholder="contoh: A, B, C, dll">
                        </div>
                    </div>
              
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea placeholder="Masukan Keterangan (opsional)" class="form-control" name="keterangan">{{ old('keterangan', $vitamin->keterangan) }}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>

@endsection
