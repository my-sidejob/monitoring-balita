@extends('layouts.app')

@section('title', 'Form Pertumbuhan Balita')

@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Data Pertumbuhan Balita</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($pertumbuhan->id)) ? route('pertumbuhan.store') : route('pertumbuhan.update', $pertumbuhan->id) }}" method="post">
                    @csrf
                    @isset($pertumbuhan->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama Balita</label>
                        <input type="hidden" name="balita_id" value="{{ $balita->id }}">
                        <input type="text" placeholder="Masukan Nama Lengkap Balita" class="form-control" name="nama_lengkap" value="{{ $balita->nama_lengkap }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" placeholder="Pilih Tanggal" class="form-control datepicker" name="tanggal" value="{{ old('tanggal', $pertumbuhan->tanggal) }}">
                    </div>
                    <div class="row">
                        <div class="col-md-6">                        
                            <div class="form-group">
                                <label class="">Berat</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="berat" value="{{ old('berat', $pertumbuhan->berat) }}" placeholder="contoh: 0.5, 1, 2, dst">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">KG</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">                        
                            <div class="form-group">       
                                <label>Tinggi</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="tinggi" value="{{ old('tinggi', $pertumbuhan->tinggi) }}" placeholder="Masukan Tinggi">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">CM</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea placeholder="Masukan Keterangan (opsional)" class="form-control" name="keterangan">{{ old('keterangan', $pertumbuhan->keterangan) }}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>

@endsection
