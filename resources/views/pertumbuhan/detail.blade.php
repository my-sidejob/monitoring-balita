@extends('layouts.app')

@section('title')
Pertumbuhan {{ $balita->nama_lengkap }}
@endsection

@section('content')
<div class="row ">  
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>{{ $balita->nama_lengkap }}</h4>
            </div>
            <div class="card-body">
                <table class="table border-top-none">
                    <tr>
                        <td>Tempat, tanggal lahir</td>
                        <td>:</td>
                        <td>{{ $balita->tempat_lahir. ', '. $balita->tanggal_lahir }}</td>
                    </tr>
                    <tr>
                        <td>Usia</td>
                        <td>:</td>
                        <td>{{ $balita->getUsia() }}</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ ($balita->jenis_kelamin == 'l') ? 'Laki - Laki' : 'Perempuan' }}</td>
                    </tr>
                    <tr>
                        <td>Orangtua</td>
                        <td>:</td>
                        <td>{{ $balita->orangtua->nama_orangtua }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $balita->orangtua->alamat }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Grafik Tinggi Balita</h4>
            </div>
            <div class="card-body">
            
                <canvas id="grafikTinggi" width="100%"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Grafik Berat Balita</h4>
            </div>
            <div class="card-body">
            
                <canvas id="grafikBerat" width="100%"></canvas>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Pertumbuhan Balita</h4>
            </div>
            <div class="card-body">
            @if(Auth::guard('web')->check())
            <a href="{{ route('pertumbuhan.create', ['balita_id' => $balita->id]) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Pertumbuhan</a>
            @endif    
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Berat</th>
                            <th>Tinggi</th>
                            <th>Keterangan</th>
                            <th>Petugas</th>
                            @if(Auth::guard('web')->check())
                            <th>Edit</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($pertumbuhan as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->tanggal }}</td>
                            <td>{{ $row->berat }} KG</td>                            
                            <td>{{ $row->tinggi }} CM</td>
                            <td>{{ $row->keterangan }}</td>
                            <td>{{ $row->user->nama }}</td>                               
                            @if(Auth::guard('web')->check())
                            <td>
                                <ul class="d-flex action-button">                                        
                                    <li><a href="{{ route('pertumbuhan.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>                                                     
                                </ul>                                
                            </td>
                            @endif
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">Belum ada data pertumbuhan</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection


@push('scripts')

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<script>
//grafik tinggi balita
var data = <?= $grafik_tinggi ?>;
// console.log(data.tinggi);

var ctx = document.getElementById('grafikTinggi').getContext('2d');
var config = {
    type: 'bar',
    data: {
        labels: data.tanggal,
        datasets: [{
            label: 'Tinggi Balita',
            data: data.tinggi,
            backgroundColor: 
                'rgba(255, 99, 132, 0.2)',
            borderColor: 
                'rgba(255, 99, 132, 1)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}
var grafikTinggi = new Chart(ctx, config);

var graph = <?= $grafik_berat ?>;

let bg = null;

let line = null;

if(graph.berat[graph.berat.length - 1] <= 5){
    bg = "rgba(255, 99, 132, 0.2)";
    line = "rgba(255, 99, 132, 1)";
}

if(graph.berat[graph.berat.length - 1] > 5 && graph.berat[graph.berat.length - 1] <= 9){
    bg = "rgba(255, 224, 153, 0.4)";
    line = "rgba(255, 224, 153, 1)";
}

if(graph.berat[graph.berat.length - 1] >= 10){
    bg = "rgba(165, 223, 223, 0.6)";
    line = "rgba(165, 223, 223, 1)";
}


var el = document.getElementById('grafikBerat').getContext('2d');

var configs = {
    type: 'line',
    data: {
        labels: graph.tanggal,
        datasets: [{
            label: 'Berat Balita',
            data: graph.berat,
            backgroundColor: 
                bg,
            borderColor: 
                line,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}

var grafikTinggi = new Chart(el, configs);


//grafik berat

</script>

@endpush