@extends('layouts.app')

@section('title', 'Pertumbuhan')

@section('content')
<div class="row ">  
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Cari Pertumbuhan Balita</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('pertumbuhan.index') }}" method="get">
                    <div class="form-group">                    
                        <label>Nama Balita</label>
                        <select name="balita_id" class="form-control mySelect">
                            <option value=""> - Pilih Balita - </option>
                            @foreach($balita as $option)
                                <option value="{{ $option->id }}">{{ $option->nama_lengkap }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm mb-4 float-right"><i class="fa fa-search"></i> Cari</a>              
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
