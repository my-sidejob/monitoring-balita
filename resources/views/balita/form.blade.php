@extends('layouts.app')

@section('title', 'Form Balita')

@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Data Balita</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($balita->id)) ? route('balita.store') : route('balita.update', $balita->id) }}" method="post">
                    @csrf
                    @isset($balita->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama Balita</label>
                        <input type="text" placeholder="Masukan Nama Lengkap Balita" class="form-control" name="nama_lengkap" value="{{ old('nama_lengkap', $balita->nama_lengkap) }}">
                    </div>
                    <div class="form-group">       
                        <label>Jenis Kelamin</label>
                        <select name="jenis_kelamin" class="form-control mySelect">
                            <option value=""> - Pilih Jenis Kelamin - </option>
                            <option value="l" {{ (old('jenis_kelamin') == 'l' || $balita->jenis_kelamin == 'l') ? 'selected' : '' }}> Laki - Laki</option>
                            <option value="p" {{ (old('jenis_kelamin') == 'p' || $balita->jenis_kelamin == 'p') ? 'selected' : '' }}> Perempuan</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6">                    
                            <div class="form-group">       
                                <label>Tempat Lahir</label>
                                <input type="text" placeholder="Masukan Tempat Lahir Balita" class="form-control" name="tempat_lahir" value="{{ old('tempat_lahir', $balita->tempat_lahir) }}">
                            </div>
                        </div>
                        <div class="col-md-6">                        
                            <div class="form-group">       
                                <label>Tanggal Lahir</label>
                                <input type="text" placeholder="Masukan Tanggal Lahir Balita" class="form-control datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir', $balita->tanggal_lahir) }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">       
                        <label>Orang Tua</label>
                        <select name="orangtua_id" class="form-control mySelect">
                            <option value=""> - Pilih Orang Tua - </option>
                            @foreach($orangtua as $select)
                                <option value="{{ $select->id }}" {{ (old('orangtua_id') == $select->id || $balita->orangtua_id == $select->id) ? 'selected' : '' }}> {{ $select->nama_orangtua }}</option>
                            @endforeach                            
                        </select>
                    </div>
                                      
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>



@endsection
