@extends('layouts.app')

@section('title', 'Balita')



@section('content')
<div class="row ">  
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Balita</h4>
            </div>
            <div class="card-body">
            <a href="{{ route('balita.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Balita</a>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Balita</th>
                            <th>J. Kel</th>
                            <th>Tempat, Tgl. Lahir</th>
                            <th>Usia</th>
                            <th>Orangtua</th>                         
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($balita as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_lengkap }}</td>
                            <td>{{ ($row->jenis_kelamin == 'l') ? 'Laki - Laki' : 'Perempuan' }}</td>
                            <td>{{ $row->tempat_lahir . ', ' . $row->tanggal_lahir }}</td>
                            <td>{{ $row->getUsia() }}</td>
                            <td>{{ $row->orangtua->nama_orangtua }}</td>             
                           
                            <td>
                                <form action="{{ route('balita.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">                                        
                                        <li><a href="{{ route('balita.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        @csrf
                                        @method('delete')
                                        {{-- <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li> --}}
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
