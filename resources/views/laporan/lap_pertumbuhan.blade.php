<table class="table datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Balita</th>
            <th>Orangtua</th>
            <th>Berat</th>
            <th>Tinggi</th>
            <th>Keterangan</th>
            <th>Petugas</th>
            @if(!isset($type))
            <th>Detail</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @forelse($data as $row)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $row->tanggal }}</td>
            <td>{{ $row->balita->nama_lengkap }}</td>
            <td>{{ $row->balita->orangtua->nama_orangtua }}</td>
            <td>{{ $row->berat }} KG</td>                            
            <td>{{ $row->tinggi }} CM</td>
            <td>{{ $row->keterangan }}</td>
            <td>{{ $row->user->nama }}</td>
            @if(!isset($type))
            <td>
            
                <ul class="d-flex action-button">                                        
                    <li><a href="{{ route('pertumbuhan.index', ['balita_id' => $row->balita_id]) }}" class="text-info" title="Detail"><i class="fa fa-search"></i></a></li>                                                     
                </ul>
        
            </td>
            @endif
        </tr>
        @empty
        <tr>
            <td colspan="6">Belum ada data pertumbuhan</td>
        </tr>
        @endforelse
    </tbody>
</table>