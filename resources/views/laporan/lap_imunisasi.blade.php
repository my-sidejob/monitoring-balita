<table class="table datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Balita</th>
            <th>Orangtua</th>
            <th>Nama Imunisasi</th>
            <th>Keterangan</th>
            <th>Petugas</th>
            @if(!isset($type))
            <th>Detail</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @forelse($data as $row)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $row->tanggal_imunisasi }}</td>
            <td>{{ $row->balita->nama_lengkap }}</td>
            <td>{{ $row->balita->orangtua->nama_orangtua }}</td>
            <td>{{ $row->jenis_imunisasi->nama_imunisasi }}</td>                            
            <td>{{ $row->keterangan }}</td>
            <td>{{ $row->user->nama }}</td> 
            @if(!isset($type))                              
            <td>
            
                <ul class="d-flex action-button">                                        
                    <li><a href="{{ route('imunisasi.index', ['balita_id' => $row->balita_id]) }}" class="text-info" title="Detail"><i class="fa fa-search"></i></a></li>                                                     
                </ul>
        
            </td>
            @endif
        </tr>
        @empty
        <tr>
            <td colspan="6">Belum ada data pertumbuhan</td>
        </tr>
        @endforelse
    </tbody>
</table>