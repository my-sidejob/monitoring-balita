<style>

* {
    font-family: "Arial";
}
/* .print-header {
    display: flex;
    align-items: center;
    font-family: "Arial";
} */

.print-header .left,
.print-header .right {
    display:inline-block;
}
.print-header p{
    line-height:1.1em;
    font-family: "Arial";
}

.img-box {
    padding: 0 40px;
}

table {
    width:100%;
}

table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}

h4 {
    margin:0;
    line-height:1.6em
}

.alamat {
    margin-top:5px;
}

.print-content {
    margin-bottom: 10px;
}

</style>

<div class="container">
    <div class="print-header">
        <div class="left">
            <div class="img-box">
                
                <img src="<?php echo public_path() . '/img/LOGO-puskesmas.png' ?>" alt="logo_puskesmas" width="100">
                
            </div>
        </div>
        <div class="right">
            <h4>Pemerintah Kabupaten Karangasem</h4>
            <h4>Dinas Kesehatan</h4>
            <h4>Puskesmas Manggis I</h4>
            <H4>Pustu Antiga</H4>
            <p class="alamat">Desa Antiga, Kec. Manggis</p>
        </div>
    </div>
    <hr>
    <div class="print-content">
        <h4>Data kunjungan {{ $jenis }} tahun {{ $tahun }}</h4>
    </div>

    @if($jenis == 'pertumbuhan')
        @include('laporan.lap_pertumbuhan')
    @endif

    @if($jenis == 'imunisasi')
        @include('laporan.lap_imunisasi')
    @endif

    @if($jenis == 'vitamin')
        @include('laporan.lap_vitamin')
    @endif



</div>