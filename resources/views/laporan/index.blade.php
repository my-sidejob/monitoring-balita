@extends('layouts.app')

@section('title', 'Laporan Kunjungan')

@section('content')
<div class="row ">  
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Cari Laporan Kunjungan Puskesmas</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('laporan.index') }}" method="get">
                    <div class="row mb-3">
                        <div class="col-md-6">                        
                            <div class="form-group">
                                <label>Jenis Kunjungan</label>
                                <select name="jenis_kunjungan" class="form-control" required>
                                    <option value=""> - Pilih Kunjungan - </option>
                                    <option value="pertumbuhan">Pertumbuhan</option>
                                    <option value="imunisasi">Imunisasi</option>
                                    <option value="vitamin">Vitamin</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Tahun</label>
                            <input type="number" name="tahun" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm mb-4 float-right"><i class="fa fa-search"></i> Cari</a>              
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@if($_GET)
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Laporan kunjungan {{ $_GET['jenis_kunjungan'] }} tahun {{ $_GET['tahun'] }}</h4>
            </div>
            <div class="card-body">
            <div class="mb-4">
                <a href="{{ route('print-kunjungan', [$_GET['jenis_kunjungan'], $_GET['tahun']] ) }}" class="btn btn-primary"><i class="fa fa-print"></i> Print Data</a>
            </div>
            
            @if($_GET['jenis_kunjungan'] == 'pertumbuhan')
                @include('laporan.lap_pertumbuhan')
            @endif

            @if($_GET['jenis_kunjungan'] == 'imunisasi')
                @include('laporan.lap_imunisasi')
            @endif

            @if($_GET['jenis_kunjungan'] == 'vitamin')
                @include('laporan.lap_vitamin')
            @endif
            
            </div>
        </div>
    </div>
</div>

@endif



@endsection
