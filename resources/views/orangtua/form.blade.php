@extends('layouts.app')

@section('title', 'Form Orang Tua')

@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Data Orangtua</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($orangtua->id)) ? route('orangtua.store') : route('orangtua.update', $orangtua->id) }}" method="post">
                    @csrf
                    @isset($orangtua->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama Orang Tua</label>
                        <input type="text" placeholder="Masukan Nama Orang Tua" class="form-control" name="nama_orangtua" value="{{ old('nama_orangtua', $orangtua->nama_orangtua) }}">
                    </div>
                    <div class="form-group">       
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Masukan alamat">{{ old('alamat', $orangtua->alamat) }}</textarea>
                    </div>
                    <div class="form-group">       
                        <label>No. Telp</label>
                        <input type="text" placeholder="Masukan No Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $orangtua->no_telp) }}">
                    </div>
                    <div class="form-group">       
                        <label>Email</label>
                        <input type="text" placeholder="Masukan Email" class="form-control" name="email" value="{{ old('email', $orangtua->email) }}">
                    </div>
                    <div class="form-group">       
                        <label>Username</label>
                        <input type="text" placeholder="Masukan Username" class="form-control" name="username" value="{{ old('username', $orangtua->username) }}">
                    </div>
                    @if(!isset($orangtua->id))
                        <div class="form-group">       
                            <label>password</label>
                            <input type="password" placeholder="Masukan password" class="form-control" name="password" value="{{ old('password', $orangtua->password) }}">
                        </div>
                    @endif
                    
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>



@endsection
