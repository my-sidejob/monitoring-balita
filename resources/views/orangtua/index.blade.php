@extends('layouts.app')

@section('title', 'Orang Tua')

@section('content')
<div class="row ">  
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Orang Tua Balita</h4>
            </div>
            <div class="card-body">
            <a href="{{ route('orangtua.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Orang Tua</a>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Ortu</th>
                            <th>Alamat</th>
                            <th>No. Telp</th>
                            <th>Email</th>
                            <th>Username</th>                         
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orangtua as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_orangtua }}</td>
                            <td>{{ $row->alamat }}</td>
                            <td>{{ $row->no_telp }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->username }}</td>
                           
                            <td>
                                <form action="{{ route('orangtua.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">                                        
                                        <li><a href="{{ route('orangtua.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        @csrf
                                        @method('delete')
                                        {{-- <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li> --}}
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
