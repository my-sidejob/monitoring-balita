@extends('layouts.app')

@section('title', 'Ubah Password')

@section('content')
<div class="row ">  
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Ubah Password Anda</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('ubah-password.update') }}" method="post">
                @csrf 

                    <div class="form-group">
                        <label>Password Lama</label>
                        <input type="password" name="old_password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password Baru</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Simpan" class="btn btn-primary float-right">
                    </div>
                
                </form>
              
            </div>
        </div>
    </div>
</div>



@endsection
