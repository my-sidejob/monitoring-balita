@extends('layouts.app')

@section('title', 'Balita Anda')



@section('content')
<div class="row ">  
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Balita Anda</h4>
            </div>
            <div class="card-body">
           
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Balita</th>
                            <th>J. Kel</th>
                            <th>Tempat, Tgl. Lahir</th>
                            <th>Usia</th>                       
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($balita as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_lengkap }}</td>
                            <td>{{ ($row->jenis_kelamin == 'l') ? 'Laki - Laki' : 'Perempuan' }}</td>
                            <td>{{ $row->tempat_lahir . ', ' . $row->tanggal_lahir }}</td>
                            <td>{{ $row->getUsia() }}</td>        
                           
                            <td>                              
                                <ul class="d-flex action-button">                                        
                                    <li><a href="{{ route('pertumbuhan-balita-anda', ['balita_id' => $row->id]) }}" class="text-primary" title="Lihat Pertumbuhan"><i class="fa fa-weight"></i></a></li>
                                    <li><a href="{{ route('imunisasi-balita-anda', ['balita_id' => $row->id]) }}" class="text-primary" title="Lihat Imunisasi"><i class="fa fa-book-medical"></i></a></li>
                                    <li><a href="{{ route('vitamin-balita-anda', ['balita_id' => $row->id]) }}" class="text-primary" title="Lihat Pemberian Vitamin"><i class="fa fa-syringe"></i></a></li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
