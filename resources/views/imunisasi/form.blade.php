@extends('layouts.app')

@section('title', 'Form Imunisasi Balita')

@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Data Imunisasi Balita</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($imunisasi->id)) ? route('imunisasi.store') : route('imunisasi.update', $imunisasi->id) }}" method="post">
                    @csrf
                    @isset($imunisasi->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama Balita</label>
                        <input type="hidden" name="balita_id" value="{{ $balita->id }}">
                        <input type="text" placeholder="Masukan Nama Lengkap Balita" class="form-control" name="nama_lengkap" value="{{ $balita->nama_lengkap }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" placeholder="Pilih Tanggal" class="form-control datepicker" name="tanggal_imunisasi" value="{{ old('tanggal', $imunisasi->tanggal_imunisasi) }}">
                    </div>
                    <div class="form-group">
                        <label>Jenis Imunisasi</label>
                        <select name="jenis_imunisasi_id" class="form-control mySelect">
                            <option value=""> - Pilih Jenis Imunisasi - </option>
                            @foreach($jenis as $option)
                                <option value="{{ $option->id }}" {{ ($option->id == old('jenis_imunisasi_id') || $option->id == $imunisasi->jenis_imunisasi_id) ? 'selected' : '' }}>{{ $option->nama_imunisasi }}</option>
                            @endforeach
                        </select>
                    </div>
               
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea placeholder="Masukan Keterangan (opsional)" class="form-control" name="keterangan">{{ old('keterangan', $imunisasi->keterangan) }}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>

@endsection
