@extends('layouts.app')

@section('title')
Imunisasi {{ $balita->nama_lengkap }}
@endsection

@section('content')
<div class="row ">  
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>{{ $balita->nama_lengkap }}</h4>
            </div>
            <div class="card-body">
                <table class="table border-top-none">
                    <tr>
                        <td>Tempat, tanggal lahir</td>
                        <td>:</td>
                        <td>{{ $balita->tempat_lahir. ', '. $balita->tanggal_lahir }}</td>
                    </tr>
                    <tr>
                        <td>Usia</td>
                        <td>:</td>
                        <td>{{ $balita->getUsia() }}</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ ($balita->jenis_kelamin == 'l') ? 'Laki - Laki' : 'Perempuan' }}</td>
                    </tr>
                    <tr>
                        <td>Orangtua</td>
                        <td>:</td>
                        <td>{{ $balita->orangtua->nama_orangtua }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $balita->orangtua->alamat }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Imunisasi Balita</h4>
            </div>
            <div class="card-body">
            @if(Auth::guard('web')->check())
            <a href="{{ route('imunisasi.create', ['balita_id' => $balita->id]) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Imunisasi</a>
            @endif    
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nama Imunisasi</th>
                            <th>Keterangan</th>
                            <th>Petugas</th>
                            @if(Auth::guard('web')->check())
                            <th>Edit</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($imunisasi as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->tanggal_imunisasi }}</td>
                            <td>{{ $row->jenis_imunisasi->nama_imunisasi }}</td>                            
                            <td>{{ $row->keterangan }}</td>              
                            <td>{{ $row->user->nama }}</td>                               
                            @if(Auth::guard('web')->check())
                            <td>
                                <ul class="d-flex action-button">                                        
                                    <li><a href="{{ route('imunisasi.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>                                                     
                                </ul>
                                
                            </td>
                            @endif
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">Belum ada data Imunisasi</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                <p><strong>Keterangan: </strong>Imunisasi dikatakan lengakap apabila sudah melakukan imunisasi Semua mulai dari imunisasi BCG, DTP, Campak, Cacar Air, Hepatitis B, Hib, Flu, MMR, Pneumokokus, Polio, dan Rotavirus.</p>
            </div>
        </div>
    </div>
</div>



@endsection
