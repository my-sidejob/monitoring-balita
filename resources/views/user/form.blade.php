@extends('layouts.app')

@section('title', 'Form User')

@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Data user</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($user->id)) ? route('user.store') : route('user.update', $user->id) }}" method="post">
                    @csrf
                    @isset($user->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama User</label>
                        <input type="text" placeholder="Masukan Nama User" class="form-control" name="nama" value="{{ old('nama', $user->nama) }}">
                    </div>                    
                    <div class="form-group">       
                        <label>Email</label>
                        <input type="text" placeholder="Masukan Email" class="form-control" name="email" value="{{ old('email', $user->email) }}">
                    </div>
                    <div class="form-group">       
                        <label>Username</label>
                        <input type="text" placeholder="Masukan Username" class="form-control" name="username" value="{{ old('username', $user->username) }}">
                    </div>
                    @if(!isset($user->id))
                        <div class="form-group">       
                            <label>password</label>
                            <input type="password" placeholder="Masukan password" class="form-control" name="password" value="{{ old('password', $user->password) }}">
                        </div>
                    @endif
                    
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>



@endsection
