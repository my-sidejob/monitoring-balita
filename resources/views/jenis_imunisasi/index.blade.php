@extends('layouts.app')

@section('title', 'Jenis Imunisasi')



@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Jenis Imunisasi</h4>
            </div>
            <div class="card-body">
            <a href="{{ route('jenis_imunisasi.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Jenis Imunisasi</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Imunisasi</th>
                            <th class="text-center">Keterangan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jenis_imunisasi as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_imunisasi }}</td>
                            <td class="text-center">{{ $row->keterangan }}</td>
                            <td>
                                <form action="{{ route('jenis_imunisasi.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">                                        
                                        <li><a href="{{ route('jenis_imunisasi.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        @csrf
                                        @method('delete')
                                        {{-- <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li> --}}
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
