<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <img src="{{ asset('img/icon.jpg') }}" alt="person" class="img-fluid rounded-circle">
                <h2 class="h5">{{ Auth::user()->username }}</h2><span>{{ (Auth::guard('parent')->check()) ? 'Orang Tua Balita' : 'Petugas Posyandu' }}</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>S</strong><strong class="text-primary">I</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->

        @if(Auth::guard('web')->check())
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">                  
                <li class="{{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}"><a href="{{ url('dashboard') }}"> <i class="icon-home"></i>Dashboard</a></li>
                <li class="{{ (urlHasPrefix('jenis_imunisasi') == true ) ? 'active' : '' }}"><a href="{{ url('jenis_imunisasi') }}"> <i class="fa fa-plus"></i>Jenis Imunisasi</a></li>
                <li class="{{ (urlHasPrefix('orangtua') == true ) ? 'active' : '' }}"><a href="{{ url('orangtua') }}"> <i class="fa fa-users"></i>Orang Tua</a></li>
                <li class="{{ (urlHasPrefix('balita') == true ) ? 'active' : '' }}"><a href="{{ url('balita') }}"> <i class="fa fa-baby"></i>Balita</a></li>
                <li class="{{ (urlHasPrefix('user') == true ) ? 'active' : '' }}"><a href="{{ url('user') }}"> <i class="fa fa-user-tie"></i>Petugas</a></li>
            </ul>
        </div>
        <div class="admin-menu mt-4">
            <h5 class="sidenav-heading">Monitoring</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled"> 
                <li class="{{ (urlHasPrefix('pertumbuhan') == true ) ? 'active' : '' }}"><a href="{{ url('pertumbuhan') }}"> <i class="fa fa-weight"></i>Pertumbuhan</a></li>
                <li class="{{ (urlHasPrefix('imunisasi') == true ) ? 'active' : '' }}"><a href="{{ url('imunisasi') }}"> <i class="fa fa-book-medical"></i>Imunisasi</a></li>
                <li class="{{ (urlHasPrefix('vitamin') == true ) ? 'active' : '' }}"><a href="{{ url('vitamin') }}"> <i class="fa fa-syringe"></i>Vitamin</a></li>                
            </ul>
        </div>
        <div class="admin-menu mt-4">
            <h5 class="sidenav-heading">REPORT</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled"> 
                <li class="{{ (urlHasPrefix('laporan') == true ) ? 'active' : '' }}"><a href="{{ url('laporan') }}"> <i class="fa fa-notes-medical"></i>Laporan</a></li>               
            </ul>
        </div>
        @else 
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">                  
                <li class="{{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}"><a href="{{ url('parent/dashboard') }}"> <i class="icon-home"></i>Dashboard</a></li>
                <li class="{{ (urlHasPrefix('balita-anda') == true ) ? 'active' : '' }}"><a href="{{ url('balita-anda') }}"> <i class="fa fa-baby"></i>Balita Anda</a></li>
                <li class="{{ (urlHasPrefix('ubah-password') == true ) ? 'active' : '' }}"><a href="{{ url('ubah-password') }}"> <i class="fa fa-key"></i>Ubah Password</a></li>
            </ul>
        </div>
        @endif

    </div>
</nav>