<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RedirectIfParentNotAuthenticated extends Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected function redirectTo($request)
    {
        if (Auth::guard('parent')->user() == null) {
            return route('parent.login');
        }
    }
}
