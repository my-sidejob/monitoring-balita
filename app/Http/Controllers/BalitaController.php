<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balita;
use App\Orangtua;
use App\Http\Requests\StoreBalita;
use Auth;

class BalitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $balita;

    public function __construct()
    {
        $this->balita = new Balita();
    }
    
    public function index()
    {
        return view ('balita.index', ['balita' => $this->balita->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['balita'] = (object) $this->balita->getDefaultValues();
        $data['orangtua'] = Orangtua::all();

        return view('balita.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBalita $request)
    {     
        Balita::create($request->all());
        return redirect()->route('balita.index')->with('success', 'Berhasil menambah data balita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['balita'] = (object) $this->balita->find($id);
        $data['orangtua'] = Orangtua::all();

        return view('balita.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBalita $request, $id)
    {
        $this->balita->find($id)->update($request->all());
        return redirect()->route('balita.index')->with('success', 'Berhasil mengubah data balita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->balita->find($id)->delete();
        return redirect()->route('balita.index')->with('success', 'Berhasil menghapus data balita');
    }


    public function parentBabies()
    {
        //dd(Auth::user()->id);
        return view('parent.balita')->with('balita', Balita::where('orangtua_id', Auth::user()->id)->get());
    }
}
