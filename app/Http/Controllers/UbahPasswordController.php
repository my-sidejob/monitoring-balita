<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;

class UbahPasswordController extends Controller
{
    public function index()
    {
        return view('parent.ubah-password');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|max:24',
            'password' => 'required|max:24|min:6|confirmed',
        ]);
        if (!(Hash::check($request->old_password, Auth::guard('parent')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Password lama anda tidak cocok!");
        }
        if(strcmp($request->old_password, $request->password) == 0){
            return redirect()->back()->with("error","Password lama dan password baru tidak boleh sama!");
        }

        $pelanggan = Auth::guard('parent')->user();
  
        $pelanggan->password = $request->password;
        $pelanggan->save();


        return redirect()->back()->with("success","Ubah Password berhasil!");
    }
}
