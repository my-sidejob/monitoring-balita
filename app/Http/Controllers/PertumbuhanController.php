<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balita;
use App\Pertumbuhan;
use App\Http\Requests\StorePertumbuhan;
use Auth;

class PertumbuhanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->balita_id == null){
            return view('pertumbuhan.index')->with('balita', Balita::orderBy('nama_lengkap', 'asc')->get());
        } else {
            $data['grafik_tinggi'] = $this->grafikTinggiBalita($request->balita_id);
            $data['grafik_berat'] = $this->grafikBeratBalita($request->balita_id);
            $data['balita'] = Balita::find($request->balita_id);
            $data['pertumbuhan'] = Pertumbuhan::where('balita_id', $request->balita_id)->orderBy('tanggal', 'desc')->get();

            if(Auth::guard('parent')->check()){
                if($data['balita']->orangtua_id != Auth::guard('parent')->user()->id){
                    return redirect()->back()->with('warning', 'Tidak dapat mengakses balita orang lain!');
                }
            }

            return view('pertumbuhan.detail', $data);
            
        }
    }

    public function grafikTinggiBalita($id)
    {
        $tinggi = Pertumbuhan::select('tinggi')->where('balita_id', $id)->orderBy('tanggal', 'asc')->get();
        $tanggal = Pertumbuhan::select('tanggal')->where('balita_id', $id)->orderBy('tanggal', 'asc')->get();
        $array = [
            'tinggi' => [
                
            ],
            'tanggal' => [

            ]
        ];

        //return $array['tinggi'];
        foreach($tinggi as $data) {
            array_push($array['tinggi'], $data->tinggi);
        }

        foreach($tanggal as $data) {
            array_push($array['tanggal'], $data->tanggal);
        }

        return json_encode($array);
    }

    public function grafikBeratBalita($id)
    {
        $berat = Pertumbuhan::select('berat')->where('balita_id', $id)->orderBy('tanggal', 'asc')->get();
        $tanggal = Pertumbuhan::select('tanggal')->where('balita_id', $id)->orderBy('tanggal', 'asc')->get();
        $array = [
            'berat' => [
                
            ],
            'tanggal' => [

            ],
            'color' => [

            ],
        ];

        //return $array['berat'];
        foreach($berat as $data) {
            array_push($array['berat'], $data->berat);
        }

        foreach($tanggal as $data) {
            array_push($array['tanggal'], $data->tanggal);
        }
        
        return json_encode($array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pertumbuhan = new Pertumbuhan();
        $data['balita'] = Balita::find($_GET['balita_id']);
        $data['pertumbuhan'] = (object) $pertumbuhan->getDefaultValues();
        return view('pertumbuhan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePertumbuhan $request)
    {
        $request['user_id'] = Auth::user()->id;
        Pertumbuhan::create($request->all());
        return redirect()->route('pertumbuhan.index', ['balita_id' => $request->balita_id])->with('success', 'Berhasil menambah data pertumbuhan balita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show()
    {

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertumbuhan = new Pertumbuhan();
        $data['pertumbuhan'] = $pertumbuhan->find($id);
        $data['balita'] = Balita::find($data['pertumbuhan']->balita_id);
        return view('pertumbuhan.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Pertumbuhan::find($id)->update($request->all());
        return redirect()->route('pertumbuhan.index', ['balita_id' => $request->balita_id])->with('success', 'Berhasil mengubah data pertumbuhan balita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
