<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orangtua;
use App\Http\Requests\StoreOrangtua;

class OrangtuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $orangtua;

    public function __construct()
    {
        $this->orangtua = new Orangtua();
    }
    
    public function index()
    {
        return view ('orangtua.index', ['orangtua' => $this->orangtua->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['orangtua'] = (object) $this->orangtua->getDefaultValues();

        return view('orangtua.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrangtua $request)
    {
        Orangtua::create($request->all());
        return redirect()->route('orangtua.index')->with('success', 'Berhasil menambah data orang tua');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['orangtua'] = (object) $this->orangtua->find($id);

        return view('orangtua.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrangtua $request, $id)
    {
        $this->orangtua->find($id)->update($request->all());
        return redirect()->route('orangtua.index')->with('success', 'Berhasil mengubah data orang tua');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->orangtua->find($id)->delete();
        return redirect()->route('orangtua.index')->with('success', 'Berhasil menghapus data Orang tua');
    }
}
