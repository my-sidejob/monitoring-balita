<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Providers\RouteServiceProvider;
use Auth;


class ParentLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/parent/dashboard';

    public function __construct()
    {
        $this->middleware('guest:parent')->except('logout');
    }

    public function showLoginForm()
    {
        return view('parent.login');
    }

    public function username()
    {
        return 'username';
    }

    protected function guard()
    {
        return Auth::guard('parent');
    }

}
