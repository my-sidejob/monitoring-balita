<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balita;
use App\Vitamin;
use App\Http\Requests\StoreVitamin;
use Auth;


class VitaminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->balita_id == null){
            return view('vitamin.index')->with('balita', Balita::orderBy('nama_lengkap', 'asc')->get());
        } else {
            $data['balita'] = Balita::find($request->balita_id);
            $data['vitamin'] = Vitamin::where('balita_id', $request->balita_id)->orderBy('tanggal', 'desc')->get();

            if(Auth::guard('parent')->check()){
                if($data['balita']->orangtua_id != Auth::guard('parent')->user()->id){
                    return redirect()->back()->with('warning', 'Tidak dapat mengakses balita orang lain!');
                }
            }
            
            return view('vitamin.detail', $data);
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vitamin = new Vitamin();
        $data['balita'] = Balita::find($_GET['balita_id']);
        $data['vitamin'] = (object) $vitamin->getDefaultValues();
        return view('vitamin.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVitamin $request)
    {
        $request['user_id'] = Auth::user()->id;
        vitamin::create($request->all());
        return redirect()->route('vitamin.index', ['balita_id' => $request->balita_id])->with('success', 'Berhasil menambah data vitamin balita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show()
    {

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vitamin = new Vitamin();
        $data['vitamin'] = $vitamin->find($id);
        $data['balita'] = Balita::find($data['vitamin']->balita_id);
        return view('vitamin.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        vitamin::find($id)->update($request->all());
        return redirect()->route('vitamin.index', ['balita_id' => $request->balita_id])->with('success', 'Berhasil mengubah data vitamin balita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
