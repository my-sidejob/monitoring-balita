<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisImunisasi;

class JenisImunisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jenis_imunisasi/index', ['jenis_imunisasi' => JenisImunisasi::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jm = new JenisImunisasi();
    
        $data['jenis_imunisasi'] = (object) $jm->getDefaultValues();
     
        return view('jenis_imunisasi/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_imunisasi' => 'required',         
        ]);

        JenisImunisasi::create($request->all());
        return redirect()->route('jenis_imunisasi.index')->with('success', 'Berhasil menambah data jenis imunisasi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['jenis_imunisasi'] = JenisImunisasi::find($id);
        return view('jenis_imunisasi.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_imunisasi' => 'required',         
        ]);

        JenisImunisasi::find($id)->update($request->all());
        return redirect()->route('jenis_imunisasi.index')->with('success', 'Berhasil mengubah data jenis imunisasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JenisImunisasi::find($id)->delete();
        return redirect()->route('jenis_imunisasi.index')->with('success', 'Berhasil menghapus data jenis imunisasi');
    }
}
