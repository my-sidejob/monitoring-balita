<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balita;
use App\Imunisasi;
use App\JenisImunisasi;
use App\Http\Requests\StoreImunsasi;
use Auth;

class ImunisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->balita_id == null){
            return view('imunisasi.index')->with('balita', Balita::orderBy('nama_lengkap', 'asc')->get());
        } else {
            $data['balita'] = Balita::find($request->balita_id);
            $data['imunisasi'] = Imunisasi::where('balita_id', $request->balita_id)->orderBy('tanggal_imunisasi', 'desc')->get();
            
            if(Auth::guard('parent')->check()){
                if($data['balita']->orangtua_id != Auth::guard('parent')->user()->id){
                    return redirect()->back()->with('warning', 'Tidak dapat mengakses balita orang lain!');
                }
            }

            return view('imunisasi.detail', $data);
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $imunisasi = new Imunisasi();
        $data['balita'] = Balita::find($_GET['balita_id']);
        $data['jenis'] = JenisImunisasi::all();
        $data['imunisasi'] = (object) $imunisasi->getDefaultValues();
        return view('imunisasi.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$request->validate([
    		'tanggal_imunisasi' => 'required',
    		'jenis_imunisasi_id' => 'required'
    	]);
        $request['user_id'] = Auth::user()->id;
        Imunisasi::create($request->all());
        return redirect()->route('imunisasi.index', ['balita_id' => $request->balita_id])->with('success', 'Berhasil menambah data imunisasi balita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $imunisasi = new Imunisasi();
        $data['imunisasi'] = $imunisasi->find($id);
        $data['jenis'] = JenisImunisasi::all();
        $data['balita'] = Balita::find($data['imunisasi']->balita_id);
        return view('imunisasi.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$request->validate([
    		'tanggal_imunisasi' => 'required',
    		'jenis_imunisasi_id' => 'required'
    	]);
        Imunisasi::find($id)->update($request->all());
        return redirect()->route('imunisasi.index', ['balita_id' => $request->balita_id])->with('success', 'Berhasil mengubah data imunisasi balita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
