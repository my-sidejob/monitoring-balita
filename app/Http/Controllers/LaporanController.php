<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertumbuhan;
use App\Imunisasi;
use App\Vitamin;
use PDF;

class LaporanController extends Controller
{
    public function index()
    {
        if(!$_GET){
            return view('laporan.index');
        } else {
            if($_GET['jenis_kunjungan'] == 'pertumbuhan'){
                $data = Pertumbuhan::where('tanggal', 'like', '%'.$_GET['tahun'].'%')->orderBy('tanggal', 'asc')->get();    
            }

            if($_GET['jenis_kunjungan'] == 'imunisasi'){
                $data = Imunisasi::where('tanggal_imunisasi', 'like', '%'.$_GET['tahun'].'%')->orderBy('tanggal_imunisasi', 'asc')->get();
            }

            if($_GET['jenis_kunjungan'] == 'vitamin'){
                $data = Vitamin::where('tanggal', 'like', '%'.$_GET['tahun'].'%')->orderBy('tanggal', 'asc')->get();    
            }

            return view('laporan.index')->with('data', $data);
        }
    }
    
    public function print($jenis, $tahun)
    {
        if($jenis == 'pertumbuhan'){
            $data = Pertumbuhan::where('tanggal', 'like', '%'.$tahun.'%')->orderBy('tanggal', 'asc')->get();
        }

        if($jenis == 'imunisasi'){
            $data = Imunisasi::where('tanggal_imunisasi', 'like', '%'.$tahun.'%')->orderBy('tanggal_imunisasi', 'asc')->get();
        }

        if($jenis == 'vitamin'){
            $data = Vitamin::where('tanggal', 'like', '%'.$tahun.'%')->orderBy('tanggal', 'asc')->get();    
        }

        $parse['type'] = 'prints';
        $parse['jenis'] = $jenis;
        $parse['tahun'] = $tahun;
        $parse['data'] = $data;
        
        $pdf = PDF::loadView('laporan.print', $parse);
        return $pdf->download('laporan-kunjungan-'.$jenis.'-'.$tahun.'.pdf');

        //return view('laporan.print', $parse)->with('data', $data);
    }
}
