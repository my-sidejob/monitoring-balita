<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisImunisasi extends Model
{
    protected $table = "jenis_imunisasi";
    protected $fillable = ['nama_imunisasi', 'keterangan'];

    public function imunisasi()
    {
        return $this->hasMany('App\Imunisasi');
    }

    public function getDefaultValues()
    {
        return [
            'nama_imunisasi' => '',
            'keterangan' => ''
        ];
    }
}
