<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertumbuhan extends Model
{
    protected $table = 'pertumbuhan';

    protected $fillable = [
        'tanggal',
        'berat',
        'tinggi',
        'keterangan',
        'balita_id',
        'user_id'
    ];

    public function balita()
    {
        return $this->belongsTo('App\Balita');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getDefaultValues()
    {
        return [
            'tanggal' => '',
            'berat' => '',
            'tinggi' => '',
            'keterangan' => '',
            'balita_id' => '',
            'user_id' => ''
        ];
    }



}
