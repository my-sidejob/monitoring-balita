<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Orangtua extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'orangtua';

    protected $fillable = [
        'nama_orangtua',
        'alamat',
        'no_telp',
        'email',
        'username',
        'password',        
    ];

    protected $hidden = ['password'];

    public function setPasswordAttribute($val)
    {
        return $this->attributes['password'] = bcrypt($val);
    }

    public function balita()
    {
        return $this->hasMany('App\Balita');
    }

    public function getDefaultValues()
    {
        return [
            'nama_orangtua' => '',
            'alamat' => '',
            'no_telp' => '',
            'email' => '',
            'username' => '',
            'password' => ''
        ];
    }
}
