<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Balita extends Model
{
    protected $table = 'balita';

    protected $fillable = [
        'nama_lengkap',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'orangtua_id',
    ];

    public function orangtua()
    {
        return $this->belongsTo('App\Orangtua');
    }

    public function getDefaultValues()
    {
        return [
            'nama_lengkap' => '',
            'jenis_kelamin' => '',
            'tempat_lahir' => '',
            'tanggal_lahir' => '',
            'orangtua_id' => '',
        ];
    }


    public function getUsia()
    {
        $tgl_lahir = new DateTime($this->tanggal_lahir);
        $now = new DateTime(date('Y-m-d'));
        $diff = $now->diff($tgl_lahir);
        $umur = $diff->y. ' Tahun '. $diff->m. ' Bulan';
        return $umur;
        // $umur = date('Y') - $tgl_lahir[0];
        // return $umur;
    }
}
