<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vitamin extends Model
{
    protected $table = 'vitamin';

    protected $fillable = [
        'nama_vitamin',
        'tanggal',
        'keterangan',
        'balita_id',
        'user_id'
    ];

    public function balita()
    {
        return $this->belongsTo('App\Balita');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getDefaultValues()
    {
        return [
            'nama_vitamin' => '',
            'tanggal' => '',
            'keterangan' => '',
            'balita_id' => '',
            'user_id' => ''
        ];
    }
}
